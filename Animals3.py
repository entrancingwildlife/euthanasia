# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.

Get data
Process data
Feature selection / engineering
Test / train / validation split
Model selection / optimisation

"""

import numpy as np
import pandas as pd
import datetime
import urllib
 
from bokeh.plotting import *
from bokeh.models import HoverTool
from collections import OrderedDict

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
# have some sql elsewhere to join intake and outcomes data
from sqlalchemy import create_engine


from sklearn import linear_model
from sklearn import cross_validation
from sklearn import metrics
from sklearn import tree
from sklearn import ensemble
from sklearn import neighbors
from sklearn import feature_selection
from sklearn import model_selection
from sklearn import svm

#%%
disk_engine = create_engine('sqlite:///AustinAnimalCenter.db')
intake_outcomes = pd.read_sql_query("SELECT * FROM intake_outcomes_full_clean WHERE (intake_animal_id || outcome_animal_id IS NOT NULL)", disk_engine)

#%%
#I'll make a reduced dataframe with a few features
intake_outcomes_euth = intake_outcomes[['outcome_type', 'intake_type','intake_condition', 'sex_upon_intake', 'age_upon_intake_years', 'intake_animal_id', 'days_at_center', 'intake_day_of_week', 'intake_week_of_year', 'intake_day', 'intake_month', 'intake_year', 'intake_animal_type' ]].copy()
intake_outcomes_euth['age_upon_intake_years'] = intake_outcomes_euth['age_upon_intake_years'].astype(float)
intake_outcomes_euth['euthanised'] = 0

intake_outcomes_euth.loc[intake_outcomes_euth['outcome_type'] == 'Euthanasia', 'euthanised'] = 1
#intake_outcomes_euth[intake_outcomes_euth['euthanised']==1]['age_upon_intake_years'].isnull().count()

#cols_to_keep = ['euthanised', 'age_upon_intake_years', 'intake_condition'
#                , 'sex_upon_intake', 'intake_type']



#%% 
# Could we put in an intermediate step to convert all categoricals into numbers
# This would help standardise the later steps

#%% Create dummy variables for categoricals
dummy_intake_condition = pd.get_dummies(intake_outcomes_euth['intake_condition'], prefix='intake_condition')
dummy_sex_upon_intake = pd.get_dummies(intake_outcomes_euth['sex_upon_intake'], prefix='sex_upon_intake')
dummy_intake_type = pd.get_dummies(intake_outcomes_euth['intake_type'], prefix='intake_type')
dummy_intake_day_of_week = pd.get_dummies(intake_outcomes_euth['intake_day_of_week'], prefix='intake_day_of_week')
dummy_intake_animal_type = pd.get_dummies(intake_outcomes_euth['intake_animal_type'], prefix='intake_animal_type')

#%% Set up dataframe for model
cols_to_keep = ['euthanised', 'age_upon_intake_years', 'intake_year', 'intake_month', 'intake_day']
data = intake_outcomes_euth[cols_to_keep].join(dummy_intake_condition.ix[:, 'intake_condition_Feral':])
data = data.join(dummy_intake_type.ix[:, 'intake_type_Owner Surrender':])
data = data.join(dummy_sex_upon_intake.ix[:, 'sex_upon_intake_Intact Male':])
data = data.join(dummy_intake_day_of_week.ix[:, 'intake_day_of_week_Monday':])
data = data.join(dummy_intake_animal_type.ix[:, 'intake_day_of_week_Cat':])

#data = intake_outcomes_euth[cols_to_keep].copy()
data['intercept'] = 1.0

#%%

# Now lets have a go at a logistic regression
#X = intake_outcomes_euth[["age_upon_intake_years"]]
X = data.iloc[:,1:2]
y = data["euthanised"]

precision_array_test = []
precision_array_train = []
test_split_array = np.linspace(0.1,0.9,9)
#test_split_array = [0.8]
for test_size in test_split_array:
    X_train, X_test, y_train, y_test =   cross_validation.train_test_split(X.values, y.values, test_size=test_size, random_state=111)
    #model = linear_model.LogisticRegression(penalty='l2', C=0.01,class_weight="balanced")
    model = ensemble.RandomForestClassifier(class_weight="balanced", bootstrap=True, criterion='entropy', max_depth=10, n_estimators=300)
    #model = ensemble.BaggingClassifier()
    #model = neighbors.KNeighborsClassifier()
    model.fit(X_train, y_train)
    average_precision_test = metrics.average_precision_score(y_test, model.predict(X_test))
    average_precision_train = metrics.average_precision_score(y_train, model.predict(X_train))
    precision_array_test.append(average_precision_test)
    precision_array_train.append(average_precision_train)
    

plt.plot(test_split_array, precision_array_test)
plt.plot(test_split_array, precision_array_train, '-x')
#%% Let's try out some classifiers.
y = data["euthanised"]

precision_array_test = []
precision_array_train = []
#test_split_array = np.linspace(0.1,0.9,36)
test_split_array = [0.7]
feature_test_range = range(2, len(data.columns))
for feature_range in feature_test_range:
    X = data.iloc[:,1:feature_range]
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X.values, y.values, test_size=test_size, random_state=111)
    #model = linear_model.LogisticRegression(penalty='l2', C=0.01,class_weight="balanced")
    #model = tree.DecisionTreeClassifier(class_weight="balanced")
    #model = ensemble.RandomForestClassifier(class_weight="balanced")
    #model = ensemble.BaggingClassifier()
    #model = neighbors.KNeighborsClassifier()
    #model = svm.SVC(kernel="linear", C=0.025)
    RFC = ensemble.RandomForestClassifier(class_weight="balanced", bootstrap=True, criterion='entropy', max_depth=10, n_estimators=300)
    model = ensemble.AdaBoostClassifier(n_estimators=100, base_estimator = RTC)
    #model = svm.SVC(gamma=2, C=1)
    model.fit(X_train, y_train)
    average_precision_test = metrics.average_precision_score(y_test, model.predict(X_test))
    average_precision_train = metrics.average_precision_score(y_train, model.predict(X_train))
    precision_array_test.append(average_precision_test)
    precision_array_train.append(average_precision_train)
    

plt.plot(feature_test_range, precision_array_test)
plt.plot(feature_test_range, precision_array_train, '-x')
#%% Do some hyperparameter stuff



parameter_candidates = { "n_estimators"      : [250, 300],
           "criterion"         : ["gini", "entropy"],
           "max_depth"         : [2, 10, 20],
           "max_features"      : [0.1, 0.25, 0.5, 0.9],
           "min_samples_split" : [2, 4] ,
           "bootstrap": [True, False]}

# Create a classifier object with the classifier and parameter candidates
grid_search_object = model_selection.GridSearchCV(ensemble.RandomForestClassifier(class_weight="balanced"), param_grid=parameter_candidates, n_jobs=-1)
grid_search_object.fit(X_train, y_train)
print(grid_search_object.best_params_)

#%% We'll run the fit again with those parameters
y = data["euthanised"]

precision_array_test = []
precision_array_train = []
auc_test_array = []
auc_train_array = []
fpr_test = []
tpr_test = []
fpr_train = []
tpr_train = []

#test_split_array = np.linspace(0.1,0.9,36)
test_split_array = [0.7]
feature_test_range = range(2, len(data.columns))
i = 0
for feature_range in feature_test_range:
    X = data.iloc[:,1:feature_range]
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X.values, y.values, test_size=test_size, random_state=111)
    #model = linear_model.LogisticRegression(penalty='l2', C=0.01,class_weight="balanced")
    #model = tree.DecisionTreeClassifier(class_weight="balanced")
    #model = ensemble.RandomForestClassifier(class_weight="balanced", bootstrap=True, criterion='gini', max_depth=10, min_samples_split=2, n_estimators=250, max_features=0.1)
    #model = ensemble.BaggingClassifier()
    #model = neighbors.KNeighborsClassifier()
    RFC = ensemble.RandomForestClassifier(class_weight="balanced", bootstrap=True, criterion='entropy', max_depth=10, n_estimators=300)
    model = ensemble.AdaBoostClassifier(n_estimators=100, base_estimator = RFC)    
    model.fit(X_train, y_train)
    average_precision_test = metrics.average_precision_score(y_test, model.predict(X_test))
    average_precision_train = metrics.average_precision_score(y_train, model.predict(X_train))
    #auc_test = metrics.auc(y_test, model.predict(X_test))
    #auc_train = metrics.auc(y_train, model.predict(X_train))
    fpr_test, tpr_test, _ = metrics.roc_curve(y_test, model.predict(X_test))
    fpr_train, tpr_train, _ = metrics.roc_curve(y_train, model.predict(X_train))
    auc_test_array.append(metrics.auc(fpr_test, tpr_test))
    auc_train_array.append(metrics.auc(fpr_train, tpr_train))
    i += 1    
    precision_array_test.append(average_precision_test)
    precision_array_train.append(average_precision_train)
    

plt.plot(feature_test_range, precision_array_test)
plt.plot(feature_test_range, precision_array_train, '-x')
plt.show()

plt.plot(feature_test_range, auc_test_array)
plt.plot(feature_test_range, auc_train_array, '-x')


#%% Performance metrics
# Let's assume for now that the above model is a good one

# Confusion matrix
# True Positive   | False Negatives
# False positives | True Negatives
# i.e. you generally want large values on the
# diagonals, and small values on the off-diagonals

y_pred = model.predict(X_test)
metrics.confusion_matrix(y_test, y_pred)

# SKLearn classification report
print(metrics.classification_report(y_test, y_pred))

# Precision - the ability of the classifier not to label as positive a sample
# that is negative. Or in other words, the proportion of positive predictions
# that were correct.

# Recall - the ability of the classifier to find all the positive samples
# (we can always make sure we only ever get actual positives by never taking
# a punt on something that we're  not sure on)


#%%
# We've identified a bunch of features, now let's refine that
rfe = feature_selection.RFE(model, 3)
rfe = rfe.fit(X_train, y_train)

# summarize the selection of the attributes
print(rfe.support_)
print(rfe.ranking_)

#%%
print('average precision', average_precision)

y_score = model.predict(X_test)
print('accuracy score')
metrics.accuracy_score(y_train, model.predict(X_train))
#metrics.confusion_matrix(y_test,y_score)