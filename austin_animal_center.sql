CREATE TABLE intake_outcomes AS
select * from outcomes_example
where animal_id = 'A664378'
limit 10;

select * from intakes_example
where animal_id = 'A729499'
where animal_id = 'A664378';

select count(*) from outcomes_example;

select count(*)
from intakes_example as t1
left join 
outcomes_example as t2
on
t1.animal_id = t2.animal_id
where t1.animal_id = 'A664378';

select * 
from intakes_example as t1
inner join 
outcomes_example as t2
on
t1.animal_id = t2.animal_id
where t1.animal_id = 'A664378';

select 
level_0
, age_upon_intake
, animal_id
, animal_type
, b
from intakes_example
limit 10;

DROP TABLE IF EXISTS intake_outcomes;
CREATE TABLE intake_outcomes AS 
SELECT 
t1.level_0
,t1.age_upon_intake
,t1.animal_id
,t1.animal_type
,t1.breed
,t1.color
,t1.datetime
,t1.datetime2
,t1.found_location
,t1."index"
,t1.intake_condition
,t1.intake_type
,t1.name
,t1.sex_upon_intake
,t2.level_0 AS outcome_level_0
,t2.age_upon_outcome
,t2.animal_id AS outcome_animal_id
,t2.animal_type    AS outcome_animal_type
,t2.breed    AS outcome_breed
,t2.color    AS outcome_color
,t2.date_of_birth
,t2.datetime    AS outcome_datetime
,t2."index" AS outcome_index
,t2.monthyear
,t2.name    AS outcome_name
,t2.outcome_subtype
,t2.outcome_type
,t2.sex_upon_outcome
from intakes_example as t1
left outer join 
outcomes_example as t2
on
t1.animal_id = t2.animal_id;

DROP TABLE IF EXISTS outcomes_intake;
CREATE TABLE outcomes_intake AS 
SELECT 
t1.level_0
,t1.age_upon_intake
,t1.animal_id
,t1.animal_type
,t1.breed
,t1.color
,t1.datetime
,t1.datetime2
,t1.found_location
,t1."index"
,t1.intake_condition
,t1.intake_type
,t1.name
,t1.sex_upon_intake
,t2.level_0 AS outcome_level_0
,t2.age_upon_outcome
,t2.animal_id AS outcome_animal_id
,t2.animal_type    AS outcome_animal_type
,t2.breed    AS outcome_breed
,t2.color    AS outcome_color
,t2.date_of_birth
,t2.datetime    AS outcome_datetime
,t2."index" AS outcome_index
,t2.monthyear
,t2.name    AS outcome_name
,t2.outcome_subtype
,t2.outcome_type
,t2.sex_upon_outcome
from outcomes_example as t2
left outer join 
intakes_example as t1
on
t1.animal_id = t2.animal_id;

DROP TABLE IF EXISTS intake_outcomes_full;
CREATE TABLE intake_outcomes_full AS 
select * from outcomes_intake
union
select * from intake_outcomes;

select count(*) from intake_outcomes_full

where intake_condition is NULL;


DROP TABLE IF EXISTS intake_outcomes_full_clean;
CREATE TABLE intake_outcomes_full_clean AS 
SELECT 
level_0
,age_upon_intake
,animal_id
,animal_type
,breed
,color
,datetime
,datetime2
,found_location
,"index"
,intake_condition
,intake_type
,name
,sex_upon_intake
,outcome_level_0
,age_upon_outcome
,outcome_animal_id
,outcome_animal_type
,outcome_breed
,outcome_color
,date_of_birth
,outcome_datetime
,outcome_index
,monthyear
,outcome_name
,outcome_subtype
,outcome_type
,sex_upon_outcome
,printf("%.2f", (abs(julianday(datetime) - julianday(outcome_datetime)))) AS days_at_center
,animal_id || 
FROM intake_outcomes_full
GROUP BY datetime;

--select * from intake_outcomes_full_clean
--where animal_id = 'A664378'
--limit 10
    

DROP TABLE IF EXISTS intake_outcomes_full_clean;
CREATE TABLE intake_outcomes_full_clean AS 
SELECT 
--level_0
age_upon_intake
,animal_id AS intake_animal_id
,animal_type AS intake_animal_type
,breed AS intake_breed
,color AS intake_color
,datetime AS intake_datetime
,datetime2 AS intake_datetime2
,found_location
,"index" AS intake_index
,intake_condition
,intake_type
,name
,sex_upon_intake
--,outcome_level_0
,age_upon_outcome
,outcome_animal_id
,outcome_animal_type
,outcome_breed
,outcome_color
,date_of_birth
,outcome_datetime
,outcome_index
,monthyear
,outcome_name
,outcome_subtype
,outcome_type
,sex_upon_outcome
,printf("%.2f", min(abs(julianday(datetime) - julianday(outcome_datetime)))) AS days_at_center
,animal_id || datetime as animal_id_datetime
,CASE WHEN age_upon_intake LIKE '%years%' THEN printf("%.2f", REPLACE(age_upon_intake, 'years', '')/1)
WHEN age_upon_intake LIKE '%year%' THEN printf("%.2f", REPLACE(age_upon_intake, 'year', '')/1)
WHEN age_upon_intake LIKE '%months%' THEN printf("%.2f", REPLACE(age_upon_intake, 'months', '')/12)
WHEN age_upon_intake LIKE '%month%' THEN printf("%.2f", REPLACE(age_upon_intake, 'month', '')/12)
WHEN age_upon_intake LIKE '%weeks%' THEN printf("%.2f", REPLACE(age_upon_intake, 'weeks', '')/52)
WHEN age_upon_intake LIKE '%week%' THEN printf("%.2f", REPLACE(age_upon_intake, 'week', '')/52)
WHEN age_upon_intake LIKE '%days%' THEN printf("%.4f", REPLACE(age_upon_intake, 'days' , '')/365)
WHEN age_upon_intake LIKE '%day%' THEN printf("%.4f", REPLACE(age_upon_intake, 'day' , '')/365)
END AS age_upon_intake_years
,CASE WHEN age_upon_outcome LIKE '%years%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'years', '')/1)
WHEN age_upon_outcome LIKE '%year%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'year', '')/1)
WHEN age_upon_outcome LIKE '%months%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'months', '')/12)
WHEN age_upon_outcome LIKE '%month%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'month', '')/12)
WHEN age_upon_outcome LIKE '%weeks%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'weeks', '')/52)
WHEN age_upon_outcome LIKE '%week%' THEN printf("%.2f", REPLACE(age_upon_outcome, 'week', '')/52)
WHEN age_upon_outcome LIKE '%days%' THEN printf("%.4f", REPLACE(age_upon_outcome, 'days' , '')/365)
WHEN age_upon_outcome LIKE '%day%' THEN printf("%.4f", REPLACE(age_upon_outcome, 'day' , '')/365)
END AS age_upon_outcome_years
, CASE CAST (strftime('%w', datetime) AS integer)
  WHEN 0 THEN 'Sunday'
  WHEN 1 THEN 'Monday'
  WHEN 2 THEN 'Tuesday'
  WHEN 3 THEN 'Wednesday'
  WHEN 4 THEN 'Thursday'
  WHEN 5 THEN 'Friday'
  WHEN 6 THEN 'Saturday'
  ELSE '' END AS intake_day_of_week
, strftime('%W', datetime) AS intake_week_of_year
, strftime('%d', datetime) AS intake_day
, strftime('%m', datetime) AS intake_month
, strftime('%Y', datetime) AS intake_year
FROM intake_outcomes_full
GROUP BY animal_id || datetime;


CREATE TABLE intake_outcomes_full_clean_bk AS
SELECT
    *
FROM intake_outcomes_full_clean;

SELECT * FROM
intake_outcomes_full_clean
LIMIT 10;

select age_upon_intake_years, age_upon_intake from intake_outcomes_full_clean
where age_upon_intake_years is null
limit 10 
--where outcome_index is NULL
where animal_id = 'A664378'

-- the clean step is too strong - there should be roughly 70k complete events. It's the group by that's losing it
select count(*) from intake_outcomes_full_clean

select count(distinct animal_id) from intake_outcomes
select count(*) from outcomes_intake
select animal_id, count( animal_id) from intake_outcomes_full_clean
group by animal_id
having count(animal_id) > 2
order by count(animal_id) desc
select count(distinct animal_id) from outcomes_example

select animal_id from outcomes_example
union
select animal_id from intakes_example